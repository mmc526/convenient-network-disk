import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  base:'./',
  server: {	
    port:3555,
    // proxy: {
    //   "/api": {
    //     target: "http://139.198.126.127:8888/",
    //     changeOrigin: true,
    //     rewrite: (path) => path.replace(/^\/api/, ""),
    //     },
  
    // }
			// ← ← ← ← ← ←
    // host: '0.0.0.0'	// ← 新增内容 ←
  },
   // 强制预构建插件包
 
  plugins: [vue()]
})
