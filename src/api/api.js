import http from './http.js'

 
export default{
	//  get请求示例
	getFileList(param){//文件列表
		return http.post(`/user/file/list`,param)
	},
	delFile(param){//删除文件
		return http.delete(`/user/file/delete`,param)
	},


 
	sendCode(param){//发送验证码
		return http.post(`/mail/code/send/register`,param)
	},
	login(param){//登录
		return http.post(`/user/login`,param)
	},
	register(param){//注册
		return http.post(`/user/register`,param)
	},
 
	// 文件上传
	uploadFile(param){
		return http.upFile('/file/upload',param)
	},
	// 文件关联
	fileToUser(param){
		return http.post('/user/repository/save',param)
	},
	// 文件名修改
	changeFileName(param){
		return http.post('/user/file/name/update',param)
	},
	// 新建文件夹
	addFolder(param){
		return http.post('user/folder/create',param)
	},
	// 移动文件
	moveFile(param){
		return http.putJson('/user/file/move',param)
	},
	
	
 
	 
}
